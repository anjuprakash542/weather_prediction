
const apiurl = "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/newdelhi"
const myapi ="?key=9CZRD6CM7N92GKKY9HRL45Y35";
const celciusConvert = "&unitGroup=metric";

const mysearch = document.getElementById("mysearch");


const searchButn = document.getElementById("searchButn");
const h1degree =document.getElementById("degH1");
const errorDiv =document.getElementById("errorDiv");
const windSpeed= document.getElementById("windSpeed");
const humidity =document.getElementById("humidity");

function errorfun() {
         errorDiv.style.display="flex"; 
         setTimeout(()=>{
            errorDiv.style.display="none";
         },2000);

         
}

function getWeather(searchValue){   

   
    fetch(`https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/${searchValue}${myapi}${celciusConvert}`)
    .then(response => {
           console.log(response);
           return response.json();
    })

    .then(data => {
        h1degree.innerHTML=data.currentConditions.temp +"°C";
        windSpeed.innerHTML=data.currentConditions.windspeed + "km/hr";
        humidity.innerHTML= data.currentConditions.humidity + "%";
    })

    .catch( error => {
       errorfun();
    })

}
searchButn.addEventListener('click',() => {
    const searchValue =mysearch.value;
    getWeather(searchValue);
})
